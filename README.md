# DeletedBot Frontend

This repo contains the code for DeletedBot's frontend.

It's made with some basic HTML, CSS and JS (jQuery).
CSS is provided by [this fork](https://github.com/lianella-gg/typora-cobalt-theme) of Cobalt theme for Typora.

This project is released under GNU GPL v3.0, read the LICENSE file to know more. Contributions are always welcome!



## How to self-host

Notice: The front-end, without a backend linked, is useless. Check out the [backend](https://codeberg.org/deletedbot/backend) repo to learn how to deploy the backend before deploying the frontend.

1. Fork this repo (if you want to deploy to Netlify) or download it (if you want to deploy to your local server)
2. Run the setup script using `python3 config.py` by answering all the questions.
   Once that's done, you can upload the updated files to whichever static hosting provider you prefer. Python is only needed for one-time configuration and is not needed to run the website into itself, once the first configuration is done. 
3. Deploy as you'd usually do. [Here's](https://www.netlify.com/blog/2016/09/29/a-step-by-step-guide-deploying-on-netlify/) an handy guide for Netlify, if you're a beginner.
4. That's it.
